#!/bin/bash

PEM_KEY=$1
if [ -z "$PEM_KEY" ]; then
  echo "Please provide path to your pem key" && exit 1
fi

SERVER_USER=admin
SERVER_URI=stilltheory.org
DEPLOY_PATH="~/app/front"

yarn build --mode production
ssh -i $PEM_KEY $SERVER_USER@$SERVER_URI "rm -rf $DEPLOY_PATH/*"
scp -i $PEM_KEY -r dist $SERVER_USER@$SERVER_URI:$DEPLOY_PATH
