import { useEffect, useState } from "react";
import { Breakpoints } from "../constants/breakpoints";

interface ChessgroundSize {
  width: number;
  height: number;
}
interface UseChessgroundSize extends ChessgroundSize {
  isFullscreen: boolean;
}

// INFO: All measured in pixels
const MAX_WIDTH = Breakpoints.LG;
const WINDOW_MARGIN = 24;
const GRID_GAP = 32;
const CHESSGROUND_RATIO_ON_GRID = 4 / 9;

export function useChessgroundSize(): UseChessgroundSize {
  const [chessgroundSize, setChessgroundSize] = useState<ChessgroundSize>({
    width: 0,
    height: 0,
  });
  const [isFullscreen, setIsFullscreen] = useState(false);

  const handleSize = () => {
    const wrapperWidth = Math.min(MAX_WIDTH, window.outerWidth) - WINDOW_MARGIN;
    const innerGridWidth = Math.floor(
      (wrapperWidth - GRID_GAP) * CHESSGROUND_RATIO_ON_GRID
    );
    const chessgroundWidth =
      wrapperWidth >= Breakpoints.MD ? innerGridWidth : wrapperWidth;

    setChessgroundSize({
      width: chessgroundWidth,
      height: chessgroundWidth,
    });
    setIsFullscreen(wrapperWidth > Breakpoints.MD);
  };

  // INFO: Set size at the first client-side load
  useEffect(() => {
    handleSize();
    window.addEventListener("resize", handleSize);

    return () => window.removeEventListener("resize", handleSize);
  }, []);

  return { ...chessgroundSize, isFullscreen };
}
