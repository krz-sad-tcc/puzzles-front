export enum Queries {
  getInitialPosition = "getInitialPosition",
  getOpeningSuggestions = "getOpeningSuggestions",
  getPositionHistory = "getPositionHistory",
  getPositionInfo = "getPositionInfo",
  getPuzzleMainMoves = "getPuzzleMainMoves",
  getPuzzleSurvivalBase = "getPuzzleSurvivalBase",
  getPuzzleSurvivalStep = "getPuzzleSurvivalStep",
}
