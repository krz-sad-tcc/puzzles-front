import { Chess as ChessJS, SQUARES } from "chess.js";
import { ChessgroundSquare, Color } from "./types";

class Chess extends ChessJS {
  availableMoves = () => {
    const dests = new Map();
    SQUARES.forEach((s) => {
      const ms = chess.moves({ square: s, verbose: true });
      if (ms.length)
        dests.set(
          s,
          ms.map((m) => m.to)
        );
    });
    return dests;
  };

  getEpd = () => {
    const fenParts = this.fen().split(" ");

    // Remove en passant field when it's not legal.
    const moves = this.moves({ verbose: true });
    if (moves.every((m) => m.flags !== "e")) {
      fenParts[3] = "-";
    }

    // Remove last 2 parts, which are move numbers.
    fenParts.splice(-2);

    return fenParts.join(" ");
  };

  isBlack = () => {
    return this.turn() === "b";
  };

  isWhite = () => {
    return this.turn() === "w";
  };

  lastMove = (): ChessgroundSquare[] | undefined => {
    if (this.history().length === 0) {
      return undefined;
    }
    const move = this.history({ verbose: true }).slice(-1)[0];
    return [move.from, move.to];
  };

  turnColor = (): Color => {
    return this.turn() === "b" ? "black" : "white";
  };
}

const chess = new Chess();

export { chess };
