import { Color } from "chessground/types";
import { DIFFICULTY_LEVELS } from "./constants/puzzle";

/**
 * Represents the 64 squares in a chess board.
 */
export type { Key as ChessgroundSquare } from "chessground/types";

/**
 * Represents a move performed in the chess tool.
 */
export type { Move as ChessToolMove } from "chess.js";

/**
 * Represents the colors of pieces: "black" | "white".
 */
export type { Color } from "chessground/types";

export enum Turn {
  Computer,
  Player,
}

/**
 * Represents the level of difficulty of a puzzle.
 */
export type DifficultyLevel = typeof DIFFICULTY_LEVELS[number];

/**
 * Represents all possible states in which a puzzle can be.
 */
export enum PuzzleStatus {
  Lost = "Lost",
  Playing = "Playing",
  Won = "Won",
}

/*
 * Represents which puzzles are available to be played.
 */
export enum PuzzleType {
  MainMoves,
  Survival,
}

/**
 * Interface of the Position objects in the database.
 */
export interface Position {
  _id: string;
  _key: string;
  _rev: string;
  eco: string;
  epd: string;
  fen: string;
  isLeaf: boolean;
  name: string;
  nGames: number;
  parentEco?: string;
  parentName?: string;
}

/**
 * Interface of the LeadsTo objects in the database.
 */
export interface Move {
  _id: string;
  _key: string;
  _rev: string;
  _from: string;
  _to: string;
  moveSan: string;
  nGames: number;
}

/**
 * Represents an advance in a chess board situation.
 * Comprises a move along with its following position.
 */
export type Advance = {
  move: Move;
  position: Position;
};

/**
 * A sequence of advances.
 */
export type History = {
  advances: Advance[];
};

/**
 * Represents a single answer to a Main Moves puzzle.
 */
export type PuzzleMainMovesAnswer = Advance;

/**
 * Represents the complete information needed by a Main Moves puzzle.
 * I.e., a position and an array of answers.
 */
export type PuzzleMainMoves = {
  position: Position;
  answers: PuzzleMainMovesAnswer[];
};

export type PuzzleSurvivalStep = {
  computerAdvance: Advance | null;
  playerAdvances: Advance[] | null;
};

/*
 * A set of configurations the user can change to create a puzzle.
 */
export type PuzzleSettings = {
  color: Color;
  level: DifficultyLevel;
  puzzleType: PuzzleType;
  rootPosition: Position;
};
