export const Breakpoints = {
  MD: 768,
  LG: 1280,
} as const;
