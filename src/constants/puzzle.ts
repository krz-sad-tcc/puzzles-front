export const MAX_ATTEMPTS = 3;

export const DIFFICULTY_LEVELS = ["easy", "medium", "hard"] as const;
