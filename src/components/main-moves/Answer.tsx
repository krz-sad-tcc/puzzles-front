import { IoCheckmarkCircle, IoCloseCircle } from "react-icons/io5";
import { PuzzleMainMovesAnswer } from "../../types";
import UIText from "../ui/UIText";

type Props = {
  answer: PuzzleMainMovesAnswer;
  isGuessed: boolean;
};

const Answer = ({ answer, isGuessed }: Props) => {
  return (
    <li
      key={answer.position._id}
      className="flex gap-4 items-center p-3 bg-white ring-1 ring-slate-700/10 rounded-lg min-h-[70px]"
    >
      <UIText
        variant="display"
        className="flex-grow-0 flex-shrink-0 w-3/12 text-xl"
      >
        {answer.move.moveSan}
      </UIText>
      <span className="flex-1 text-xs flex-col inline-flex">
        <UIText variant="dimmed">{`${answer.move.nGames} games`}</UIText>
        {answer.position.name && answer.position.eco ? (
          <UIText variant="dimmed">
            <b className="font-bold inline-block mr-1">{answer.position.eco}</b>
            {answer.position.name}
          </UIText>
        ) : null}
      </span>
      {isGuessed ? (
        <IoCheckmarkCircle size={20} className="text-green-500" />
      ) : (
        <IoCloseCircle size={20} className="text-red-500" />
      )}
    </li>
  );
};

export default Answer;
