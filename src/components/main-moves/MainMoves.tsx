import { useRef, useState } from "react";
import { useQuery } from "react-query";
import { chess } from "../../chess";
import {
  Advance,
  ChessToolMove,
  History,
  PuzzleMainMoves,
  PuzzleSettings,
  PuzzleStatus,
} from "../../types";
import { getPositionHistory, getPuzzleMainMoves } from "../../api";
import { Queries } from "../../queries";
import { MAX_ATTEMPTS } from "../../constants/puzzle";
import AnswersPanel from "./AnswersPanel";
import Board from "../commons/Board";
import ControlPanel from "../commons/ControlPanel";
import HistoryPanel from "../commons/HistoryPanel";
import ReferencePanel from "../commons/ReferencePanel";
import UIPuzzleWon from "../ui/UIPuzzleWon";
import UIPuzzleLost from "../ui/UIPuzzleLost";
import UIBox from "../ui/UIBox";

type Props = PuzzleSettings;

const MainMoves = (puzzleSettings: Props) => {
  const [autoRefetch, setAutoRefetch] = useState(true);
  const [attempts, setAttempts] = useState(MAX_ATTEMPTS);
  const [fen, setFen] = useState("");
  const [guessedMoves, setGuessedMoves] = useState<boolean[]>([]);
  const [history, setHistory] = useState<History>();
  const [puzzle, setPuzzle] = useState<PuzzleMainMoves>();
  const [puzzleStatus, setPuzzleStatus] = useState<PuzzleStatus>();
  const [timeTravelAdvance, setTimeTravelAdvance] = useState<Advance | null>(
    null
  );

  const epd = useRef<string>("");

  const puzzleQuery = useQuery(
    [
      Queries.getPuzzleMainMoves,
      puzzleSettings.level,
      puzzleSettings.color,
      puzzleSettings.rootPosition._id,
    ],
    () =>
      getPuzzleMainMoves({
        color: puzzleSettings.color,
        level: puzzleSettings.level,
        rootEpd: puzzleSettings.rootPosition.epd,
      }),
    {
      onSuccess: (puzzle) => {
        epd.current = puzzle.position.epd;
        historyQuery.refetch();
        startPuzzle(puzzle);
      },
    }
  );

  const historyQuery = useQuery(
    [Queries.getPositionHistory],
    () => getPositionHistory({ epd: epd.current }),
    {
      onSuccess: setHistory,
      enabled: false,
    }
  );

  function startPuzzle(puzzle: PuzzleMainMoves) {
    chess.load(puzzle.position.fen);
    setAttempts(MAX_ATTEMPTS);
    setFen(puzzle.position.fen);
    setGuessedMoves(puzzle.answers.map((_) => false));
    setPuzzleStatus(PuzzleStatus.Playing);
    setPuzzle(puzzle);
    setTimeTravelAdvance(null);
  }

  function handleMove(move: ChessToolMove) {
    if (!puzzle) return;

    setFen(chess.fen());

    const newGuessedMoves = [...guessedMoves];
    const index = puzzle.answers.findIndex((c) => c.move.moveSan === move.san);

    if (index !== -1) {
      newGuessedMoves[index] = true;
      setGuessedMoves(newGuessedMoves);

      // Gets a new puzzle if user got everything right.
      if (newGuessedMoves.every((g) => g)) {
        setPuzzleStatus(PuzzleStatus.Won);

        if (autoRefetch) {
          setTimeout(puzzleQuery.refetch, 1400);
        }
        return;
      }
    } else {
      if (attempts === 1) {
        setPuzzleStatus(PuzzleStatus.Lost);

        // Duplicated to ensure attempts exhaustion while preventing undo.
        setAttempts((prev) => prev - 1);

        if (autoRefetch) {
          setTimeout(puzzleQuery.refetch, 1400);
        }
        return;
      }

      setAttempts((prev) => prev - 1);
    }

    // Unless the puzzle is finished, undo the last move.
    setTimeout(() => {
      chess.undo();
      setFen(chess.fen());
    }, 700);
  }

  function handleToggleAutoRefetch() {
    setAutoRefetch(!autoRefetch);
  }

  function handleNewPuzzle() {
    puzzleQuery.refetch();
  }

  function handleRestartPuzzle() {
    if (!puzzle) return;
    startPuzzle(puzzle);
  }

  function handleGoBackInHistory(advance: Advance) {
    if (advance.position.fen === fen) {
      setTimeTravelAdvance(null);
    } else {
      setTimeTravelAdvance(advance);
    }
  }

  console.log(puzzleStatus);

  return (
    <div>
      {puzzleQuery.isError ? (
        <div>Error fetching puzzle</div>
      ) : (
        <div className="grid gap-4 md:grid-cols-[2fr_4fr_3fr]">
          <div className="flex flex-col gap-4">
            <ReferencePanel position={puzzle?.position} />
            {puzzleStatus === PuzzleStatus.Won ? (
              <UIPuzzleWon onRestartPuzzle={handleRestartPuzzle}>
                <p>You got all the answers!</p>
              </UIPuzzleWon>
            ) : puzzleStatus === PuzzleStatus.Lost ? (
              <UIPuzzleLost onRestartPuzzle={handleRestartPuzzle}>
                <p>You made 3 mistakes, but don&apos;t give up!</p>
              </UIPuzzleLost>
            ) : null}
          </div>
          <Board
            fen={timeTravelAdvance?.position?.fen || fen}
            isLocked={
              puzzleStatus !== PuzzleStatus.Playing ||
              Boolean(timeTravelAdvance)
            }
            lastMove={timeTravelAdvance ? undefined : chess.lastMove()}
            onMove={handleMove}
            orientation={puzzleSettings.color}
          />
          <aside>
            <UIBox className="flex flex-col">
              <HistoryPanel
                advances={history?.advances || []}
                currEpd={timeTravelAdvance?.position?.epd || chess.getEpd()}
                onClickItem={handleGoBackInHistory}
              />
              <hr className="mb-2" />
              <AnswersPanel
                answers={puzzle ? puzzle.answers : []}
                attempts={attempts}
                guessedMoves={guessedMoves}
                showAllAnswers={puzzleStatus !== PuzzleStatus.Playing}
              />
              <hr className="my-2" />
              <ControlPanel
                autoRefetch={autoRefetch}
                buttonText="New puzzle"
                onAutoToggleRefetch={handleToggleAutoRefetch}
                onClick={handleNewPuzzle}
              />
            </UIBox>
          </aside>
        </div>
      )}
    </div>
  );
};

export default MainMoves;
