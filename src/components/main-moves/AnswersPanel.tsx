import { useChessgroundSize } from "../../hooks/useChessgroundSize";
import { PuzzleMainMovesAnswer } from "../../types";
import Answer from "./Answer";
import Attempts from "../commons/Attempts";
import UIMovePlaceholder from "../ui/UIMovePlaceholder";

type Props = {
  answers: PuzzleMainMovesAnswer[];
  attempts: number;
  guessedMoves: boolean[];
  showAllAnswers: boolean;
};

const AnswersPanel = ({
  answers,
  attempts,
  guessedMoves,
  showAllAnswers,
}: Props) => {
  const { height, isFullscreen } = useChessgroundSize();

  return (
    <div
      className="p-3 md:overflow-auto"
      style={isFullscreen ? { maxHeight: height + "px" } : {}}
    >
      <div className="md:flex md:flex-col">
        <Attempts attempts={attempts} />

        {/* <ul className="flex flex-col gap-2 mt-5"> */}
        <ul className="grid gap-2 mt-5">
          {answers.map((p, index) =>
            showAllAnswers || guessedMoves[index] ? (
              <Answer
                key={p.position._id}
                answer={p}
                isGuessed={guessedMoves[index]}
              />
            ) : (
              <UIMovePlaceholder key={p.position._id} answer={p} />
            )
          )}
        </ul>
      </div>
    </div>
  );
};

export default AnswersPanel;
