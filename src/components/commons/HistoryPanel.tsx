import { Advance } from "../../types";
import { useEffect, useRef, useState } from "react";
import HistoryItem from "./HistoryItem";
import UIText from "../ui/UIText";

type Props = {
  advances: Advance[];
  currEpd: string;
  onClickItem: (advance: Advance) => void;
};

const HistoryPanel = ({ advances, currEpd, onClickItem }: Props) => {
  const [selectedItemRow, setSelectedItemRow] = useState<number>();

  const bottomRef = useRef<HTMLDivElement | null>(null);

  const whiteAdvances = [] as Advance[];
  const blackAdvances = [] as Advance[];

  for (let i = 0; i < advances.length; i++) {
    if (i % 2 === 0) {
      whiteAdvances.push(advances[i]);
    } else {
      blackAdvances.push(advances[i]);
    }
  }

  useEffect(() => {
    bottomRef.current?.scrollIntoView({ behavior: "smooth", block: "end" });
    setSelectedItemRow(Math.ceil(advances.length / 2) - 1);
  }, [advances]);

  function handleClickItem(advance: Advance, index: number) {
    onClickItem(advance);
    setSelectedItemRow(index);
  }

  return (
    <div className="rounded-t-lg md:overflow-auto">
      <div className="max-h-[145px] overflow-y-auto bg-black/10 scroll-smooth">
        {!whiteAdvances.length ? (
          <div
            className="grid grid-cols-[45px_minmax(0,_1fr)] gap-[1px] border-t first:border-none"
            key={"history-empty"}
          >
            <div className="h-full w-full min-h-[40px] flex items-center pl-5 pr-3 text-sm bg-slate-50 text-slate-400/75">
              1
            </div>
            <UIText
              className="flex items-center h-full ml-3 italic text-sm"
              variant="dimmed"
            >
              Empty history
            </UIText>
          </div>
        ) : null}
        {whiteAdvances.map((_, index) => (
          <div
            className="grid grid-cols-[45px_minmax(0,_1fr)_minmax(0,_1fr)] gap-[1px] border-t first:border-none"
            key={"history-row" + index}
          >
            <div className="h-full w-full flex items-center pl-5 pr-3 text-sm bg-slate-50 text-slate-400/75">
              {index + 1}
            </div>
            <HistoryItem
              advance={whiteAdvances[index]}
              isSelected={
                whiteAdvances[index].position.epd === currEpd &&
                selectedItemRow === index
              }
              onClick={(advance) => handleClickItem(advance, index)}
            />
            {blackAdvances[index] ? (
              <HistoryItem
                advance={blackAdvances[index]}
                isSelected={
                  blackAdvances[index].position.epd === currEpd &&
                  selectedItemRow === index
                }
                onClick={(advance) => handleClickItem(advance, index)}
              />
            ) : null}
          </div>
        ))}
        <div ref={bottomRef}></div>
      </div>
    </div>
  );
};

export default HistoryPanel;
