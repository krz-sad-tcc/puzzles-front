import { IoCopyOutline } from "react-icons/io5";
import { useChessgroundSize } from "../../hooks/useChessgroundSize";
import { Position } from "../../types";
import UIBox from "../ui/UIBox";
import UIButton from "../ui/UIButton";
import UIText from "../ui/UIText";

const copyToClipboard = (text: string) => {
  const el = document.createElement("textarea");
  el.value = text;
  el.setAttribute("readonly", "");
  el.style.position = "absolute";
  el.style.left = "-9999999px";

  const container = document.body;
  container.appendChild(el);
  document.getSelection().removeAllRanges();
  el.focus();
  el.select();
  document.execCommand("copy");
  container.removeChild(el);
};

type Props = {
  position: Position | undefined;
};

const ReferencePanel = ({ position }: Props) => {
  const { height, isFullscreen } = useChessgroundSize();

  const title = position
    ? position.parentEco
      ? "Position derived from"
      : "Position"
    : "Unkown position";

  const eco = position
    ? position.parentEco
      ? position.parentEco
      : position.eco
    : "";
  const name = position
    ? position.parentName
      ? position.parentName
      : position.name
    : "";
  const nGamesText = position ? `${position.nGames} games` : "";

  return (
    <UIBox
      className="md:overflow-auto"
      style={isFullscreen ? { maxHeight: height + "px" } : {}}
    >
      <div className="p-4">
        <UIText variant="primary">{title}</UIText>
        <UIText variant="display" className="font-medium leading-normal mb-1">
          {eco && <b className="font-bold inline-block mr-1">{eco}</b>}
          {name}
        </UIText>
        <UIText variant="dimmed" className="font-medium leading-normal">
          {nGamesText}
        </UIText>
      </div>

      {position?.fen && (
        <div className="mx-2 mb-2 pl-2 pr-1 py-1 flex gap-2 rounded-md ring-1 ring-slate-700/10 bg-slate-200/25 text-slate-500">
          <input
            className="bg-transparent flex-grow text-ellipsis"
            value={position.fen}
            aria-label="FEN"
            readOnly
          />
          <UIButton
            className="rounded bg-white"
            title="Copy FEN"
            size="small"
            onClick={() => copyToClipboard(position.fen)}
          >
            <IoCopyOutline size={16} />
          </UIButton>
        </div>
      )}
    </UIBox>
  );
};

export default ReferencePanel;
