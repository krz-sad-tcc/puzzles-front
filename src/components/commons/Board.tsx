import { chess } from "../../chess";
import { ChessgroundSquare, ChessToolMove, Color } from "../../types";
import { useChessgroundSize } from "../../hooks/useChessgroundSize";
import Chessground from "@react-chess/chessground";

import "../../assets/chessground.css";
import "chessground/assets/chessground.base.css";
import "chessground/assets/chessground.cburnett.css";

type Props = {
  fen: string;
  isLocked: boolean;
  lastMove: ChessgroundSquare[] | undefined;
  onMove: (move: ChessToolMove) => void;
  orientation: Color;
};

const Board = ({ fen, isLocked, lastMove, onMove, orientation }: Props) => {
  const { width, height } = useChessgroundSize();

  const handleMove = (from: ChessgroundSquare, to: ChessgroundSquare) => {
    if (from === "a0" || to === "a0") {
      console.error("Unknown move: either `from` or `to` is `a0`.");
      return;
    }

    const move = chess.move({ from, to });
    if (move) {
      onMove(move);
    }
  };

  return (
    <Chessground
      width={width}
      height={height}
      config={{
        check: chess.in_check(),
        events: { move: handleMove },
        fen: fen,
        lastMove: lastMove,
        movable: {
          dests: isLocked ? new Map() : chess.availableMoves(),
          free: false,
        },
        orientation: orientation,
        turnColor: chess.turnColor(),
      }}
    />
  );
};

export default Board;
