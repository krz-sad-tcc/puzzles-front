import { IoRefreshCircleSharp } from "react-icons/io5";
import UIButton from "../ui/UIButton";
import UIToggleSwitch from "../ui/UIToggleSwitch";

type Props = {
  autoRefetch?: boolean;
  buttonText: string;
  onAutoToggleRefetch?: () => void;
  onClick: () => void;
};

const ControlPanel = ({
  autoRefetch,
  buttonText,
  onAutoToggleRefetch,
  onClick,
}: Props) => (
  <div className="p-3 md:overflow-auto">
    <div className="flex flex-col gap-4">
      {onAutoToggleRefetch ? (
        <div className="flex flex-col">
          <UIToggleSwitch
            enabled={autoRefetch}
            label="Go to the next puzzle automatically"
            name="auto-finish-switch"
            onToggle={onAutoToggleRefetch}
          />
        </div>
      ) : null}
      <UIButton onClick={onClick}>
        <IoRefreshCircleSharp size={24} />
        {buttonText}
      </UIButton>
    </div>
  </div>
);

export default ControlPanel;
