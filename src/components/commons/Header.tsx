const Header = () => {
  return (
    <header className="max-w-screen-xl mx-auto flex flex-col gap-4 mt-2 mb-2 sm:gap-1 sm:flex-row sm:justify-between sm:mb-3 sm:mt-3">
      <h1 className="text-2xl font-bold">Still Theory</h1>
    </header>
  );
};

export default Header;
