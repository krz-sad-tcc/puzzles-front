import { RiGitlabFill } from "react-icons/ri";
import { SiLichess } from "react-icons/si";

const Footer = () => {
  return (
    <footer className="text-sm flex flex-col mt-16 p-12 bg-gray-200 ring-slate-50 gap-2">
      <div className="flex">
        <div className="flex gap-2">
          <SiLichess size={20} />
          Based on&nbsp;
        </div>
        <a
          className="underline text-blue-600 hover:text-blue-800"
          href="https://database.lichess.org/"
          target="_blank"
          rel="noreferrer"
        >
          Lichess
        </a>
        &apos;s free databases.
      </div>
      <div className="flex">
        <div className="flex gap-2">
          <RiGitlabFill size={20} />
          Contribute with Still Theory on&nbsp;
        </div>
        <a
          className="underline text-blue-600 hover:text-blue-800"
          href="https://gitlab.com/krz-sad-tcc"
          target="_blank"
          rel="noreferrer"
        >
          GitLab
        </a>
        .
      </div>
    </footer>
  );
};

export default Footer;
