import classNames from "classnames";
import { IoHeart, IoHeartOutline } from "react-icons/io5";
import { MAX_ATTEMPTS } from "../../constants/puzzle";

type Props = {
  className?: string;
  attempts: number;
};

const Attempts = ({ className, attempts }: Props) => {
  const spent = MAX_ATTEMPTS - attempts;

  const hearts = [];
  for (let i = 0; i < spent; i++) {
    hearts.push(
      <IoHeartOutline key={"spent-" + i} className="text-pink-700" size={24} />
    );
  }
  for (let i = 0; i < attempts; i++) {
    hearts.push(
      <IoHeart key={"attempt-" + i} className="text-pink-700" size={24} />
    );
  }

  return <div className={classNames("flex gap-2", className)}>{hearts}</div>;
};

export default Attempts;
