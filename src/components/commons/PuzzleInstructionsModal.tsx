import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import UIButton from "../ui/UIButton";
import UICallout from "../ui/UICallout";

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

const PuzzleInstructionsModal = ({ isOpen, onClose }: Props) => {
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-lg transform overflow-hidden rounded-2xl bg-white p-7 text-left align-middle shadow-xl transition-all">
                <div>
                  <h3 className="text-md font-semibold mt-10">Main Moves</h3>
                  <p className="text-gray-500 mt-3">
                    Based on your preferences, a random position is chosen for
                    you. Your job is to play the main theoretical moves in that
                    position.
                  </p>
                  <p className="text-gray-500 mt-3">
                    To make things more interesting, multiple positions can be
                    based on the same root position.
                  </p>
                  <p className="text-gray-500 mt-3">
                    For example, if you choose to play with{" "}
                    <i>Sicilian Defense: Old Sicilian</i>, either it or any
                    position that would come after it can be returned.
                  </p>
                  <UICallout>
                    Note to the CS people: here the game graph is explored in a
                    breadth-first manner.
                  </UICallout>

                  <h3 className="text-md font-semibold mt-10">Survival</h3>
                  <p className="text-gray-500 mt-3">
                    You start exactly at the position you chose. Your job is to
                    get as far as possible in the game, always playing the main
                    line. That implies playing only the{" "}
                    <b>single most popular</b> move on every turn.
                  </p>
                  <UICallout>
                    Note to the CS people: here the game graph is explored in a
                    depth-first manner.
                  </UICallout>
                </div>
                <div className="mt-10 flex justify-center">
                  <UIButton type="button" onClick={onClose}>
                    Got it, thanks!
                  </UIButton>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default PuzzleInstructionsModal;
