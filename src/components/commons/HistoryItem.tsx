import classNames from "classnames";
import { Advance } from "../../types";

type Props = {
  advance: Advance;
  isSelected: boolean;
  onClick: (advance: Advance) => void;
};

const HistoryItem = ({ advance, isSelected, onClick }: Props) => {
  const classes = classNames(
    "px-5 py-2 cursor-pointer bg-white text-slate-500 hover:bg-slate-200 transition-bg duration-100",
    {
      "bg-slate-100 font-bold": isSelected,
    }
  );

  return (
    <button className={classes} onClick={() => onClick(advance)}>
      {advance.move.moveSan}
    </button>
  );
};

export default HistoryItem;
