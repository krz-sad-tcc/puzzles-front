import "../../assets/position-info.css";

type Props = {
  info: string;
};

const PositionInfo = ({ info }: Props) => {
  return (
    <div
      className="position-info mt-5 text-slate-800 text-sm leading-normal"
      dangerouslySetInnerHTML={{ __html: info }}
    />
  );
};

export default PositionInfo;
