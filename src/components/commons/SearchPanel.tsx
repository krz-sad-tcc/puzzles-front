import { useState } from "react";
import { useQuery } from "react-query";
import { Color, Position } from "../../types";
import { getOpeningSuggestions } from "../../api";
import { Queries } from "../../queries";
import UIBox from "../ui/UIBox";
import UIButton from "../ui/UIButton";
import UIInput from "../ui/UIInput";
import UILoading from "../ui/UILoading";
import UIRadio from "../ui/UIRadio";
import UIRadioGroup from "../ui/UIRadioGroup";
import UIText from "../ui/UIText";

type Props = {
  submitButtonText: string;
  chosenColor: Color | undefined;
  chosenPosition: Position | undefined;
  onChangeColor: (color: Color) => void;
  onChangePosition: (position: Position) => void;
  onStartPuzzle: () => void;
};

const SearchPanel = ({
  submitButtonText,
  chosenColor,
  chosenPosition,
  onChangeColor,
  onChangePosition,
  onStartPuzzle,
}: Props) => {
  const [term, setTerm] = useState<string>("");
  const [suggestions, setSuggestions] = useState<Position[]>();

  const suggestionsQuery = useQuery(
    Queries.getOpeningSuggestions,
    () => getOpeningSuggestions({ term }),
    { enabled: false, onSuccess: setSuggestions }
  );

  const handleSearch = (evt: React.FormEvent) => {
    evt.preventDefault();
    if (term) {
      suggestionsQuery.refetch();
    }
  };

  const handlePuzzleStart = () => {
    setSuggestions(undefined);
    setTerm("");
    onStartPuzzle();
  };

  return (
    <UIBox>
      <UIText className="p-4" variant="primary">
        Search Opening
      </UIText>

      <form className="px-4 mb-4 flex flex-col gap-3" onSubmit={handleSearch}>
        <UIInput
          ariaLabel="Search by opening name"
          id="opening-name"
          value={term}
          onChange={(evt: React.ChangeEvent<HTMLInputElement>) =>
            setTerm(evt.target.value)
          }
        />
        <UIButton onClick={handleSearch} size="small" type="submit">
          Search
        </UIButton>
      </form>

      {suggestions && (
        <>
          <hr className="mt-4" />
          <div className="p-4 flex flex-col">
            {suggestionsQuery.isFetching ? (
              <div className="m-auto">
                <UILoading />
              </div>
            ) : suggestions.length === 0 ? (
              <UIText className="text-sm mt-1" variant="dimmed">
                No match to your search.
              </UIText>
            ) : (
              <div className="flex flex-col gap-3">
                <UIText className="text-sm font-medium mt-1" variant="dimmed">
                  {suggestions.length} results
                </UIText>
                <div className="flex flex-col gap-3 ml-[-0.25rem] max-h-[250px] overflow-auto">
                  {suggestions.map((p) => (
                    <UIRadio
                      checked={chosenPosition?.epd === p.epd}
                      className="pl-1"
                      id={"opening-" + p._id}
                      key={"sugg-" + p._id}
                      name="selected-opening"
                      onChange={() => onChangePosition(p)}
                      variant="horizontal"
                    >
                      <div key={p._id} className="flex flex-col text-sm">
                        <span className="font-semibold text-slate-600">
                          {p.name}
                        </span>
                        <span className="text-slate-500">{p.nGames} games</span>
                      </div>
                    </UIRadio>
                  ))}
                </div>
                <UIRadioGroup
                  checked={chosenColor || ""}
                  id="opening-color"
                  label="Play with"
                  options={[
                    { id: "white", label: "White" },
                    { id: "black", label: "Black" },
                  ]}
                  onChange={(newColor) => onChangeColor(newColor as Color)}
                />
                <UIButton
                  onClick={handlePuzzleStart}
                  size="small"
                  type="button"
                >
                  {submitButtonText}
                </UIButton>
              </div>
            )}
          </div>
        </>
      )}
    </UIBox>
  );
};

export default SearchPanel;
