import { Combobox, Transition } from "@headlessui/react";
import { Fragment, useRef, useState } from "react";
import { IoSearch } from "react-icons/io5";
import { useQuery } from "react-query";
import { getOpeningSuggestions } from "../../api";
import { Queries } from "../../queries";
import { Position } from "../../types";
import classNames from "classnames";
import UILoading from "../ui/UILoading";
import UIText from "../ui/UIText";

type Props = {
  className?: string;
  onChangePosition: (position: Position) => void;
  rootPosition: Position;
};

const SearchBar = ({ className, onChangePosition, rootPosition }: Props) => {
  const [term, setTerm] = useState<string>("");
  const [suggestions, setSuggestions] = useState<Position[]>([rootPosition]);
  const timeoutRef = useRef(null);

  const suggestionsQuery = useQuery(
    Queries.getOpeningSuggestions,
    () => {
      return term ? getOpeningSuggestions({ term }) : [];
    },
    { enabled: false, onSuccess: setSuggestions }
  );

  const debouncedHandleSearch = (newTerm: string) => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    timeoutRef.current = setTimeout(suggestionsQuery.refetch, 500);
    setTerm(newTerm);
  };

  const positionName = rootPosition?.name;

  return (
    <div className={className}>
      <Combobox
        value={rootPosition}
        onChange={(p) => {
          onChangePosition(p);
        }}
      >
        <div className="relative flex w-full cursor-default rounded-md border border-gray-300 bg-white py-2 pl-3 pr-10 text-left shadow-sm focus-within:border-pink-500 focus-within:outline-none focus-within:ring-1 focus-within:ring-pink-500 sm:text-sm">
          <span className="text-slate-500">Puzzle root:</span>
          <Combobox.Input
            className="outline-none flex-grow ml-1 font-medium"
            onChange={(event) => debouncedHandleSearch(event.target.value)}
            value={term}
            displayValue={() => positionName}
          />
          <Combobox.Button className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
            <IoSearch
              className={classNames(
                "h-5 w-5 text-slate-400/70 transition ease-in-out duration-100"
              )}
              aria-hidden="true"
            />
          </Combobox.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Combobox.Options className="absolute origin-top-left z-10 mt-1 max-h-56 w-2/3 overflow-auto rounded-md bg-white p-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
            {suggestionsQuery.isFetching ? (
              <div className="m-auto">
                <UILoading />
              </div>
            ) : suggestions.length === 0 ? (
              <UIText className="text-sm p-2" variant="dimmed">
                No match to your search.
              </UIText>
            ) : (
              suggestions.map((position) => (
                <Combobox.Option
                  key={position._id}
                  value={position}
                  as={Fragment}
                >
                  {({ active }) => (
                    <li
                      className={classNames(
                        active
                          ? "text-pink-800 bg-pink-200/50 rounded"
                          : "text-gray-900",
                        "relative cursor-default select-none py-2 pr-3 pl-6"
                      )}
                    >
                      {position.name}
                    </li>
                  )}
                </Combobox.Option>
              ))
            )}
          </Combobox.Options>
        </Transition>
      </Combobox>
    </div>
  );
};

export default SearchBar;
