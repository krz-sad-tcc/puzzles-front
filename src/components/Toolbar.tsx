import { useState } from "react";
import { DIFFICULTY_LEVELS } from "../constants/puzzle";
import { Color, PuzzleSettings, PuzzleType } from "../types";
import { IoInformationCircleOutline } from "react-icons/io5";
import UIDropdown from "./ui/UIDropdown";
import UIRadioGroup from "./ui/UIRadioGroup";
import SearchBar from "./commons/SearchBar";
import PuzzleInstructionsModal from "./commons/PuzzleInstructionsModal";

type Props = {
  puzzleSettings: PuzzleSettings;
  onChangeSettings: (newSettings: PuzzleSettings) => void;
};

const COLOR_OPTIONS = [
  { id: "white", label: "White" },
  { id: "black", label: "Black" },
];
const LEVEL_OPTIONS = DIFFICULTY_LEVELS.map((l) => ({ label: l, value: l }));
const PUZZLE_TYPE_OPTIONS = [
  {
    label: "Main Moves",
    value: PuzzleType.MainMoves,
  },
  {
    label: "Survival",
    value: PuzzleType.Survival,
  },
];

const Toolbar = ({ puzzleSettings, onChangeSettings }: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <>
      <div className="flex flex-col gap-3 sm:flex-row">
        <SearchBar
          className="flex-grow"
          onChangePosition={(newPosition) =>
            onChangeSettings({ ...puzzleSettings, rootPosition: newPosition })
          }
          rootPosition={puzzleSettings.rootPosition}
        />
        <UIRadioGroup
          checked={puzzleSettings.color || ""}
          id="opening-color"
          options={COLOR_OPTIONS}
          onChange={(newColor) =>
            onChangeSettings({ ...puzzleSettings, color: newColor as Color })
          }
        />
      </div>
      <div className="mt-3 flex flex-col gap-3 sm:flex-row sm:items-center">
        <UIDropdown
          className="col-span-2"
          label="Difficulty"
          options={LEVEL_OPTIONS}
          value={puzzleSettings.level}
          onChange={(newLevel) =>
            onChangeSettings({ ...puzzleSettings, level: newLevel })
          }
        />
        <div className="flex items-center gap-1">
          <UIDropdown
            label="Puzzle Type"
            options={PUZZLE_TYPE_OPTIONS}
            value={puzzleSettings.puzzleType}
            onChange={(newType) =>
              onChangeSettings({ ...puzzleSettings, puzzleType: newType })
            }
          />
          <button
            className="rounded-full p-2 transition hover:bg-slate-700/20 hover:outline-none focus:outline-none focus:ring-4 focus:ring-slate-200/50 focus:bg-slate-300/50 active:bg-slate-400/50"
            onClick={() => setIsModalOpen(true)}
          >
            <IoInformationCircleOutline size={16} />
          </button>
        </div>
        <PuzzleInstructionsModal
          isOpen={isModalOpen}
          onClose={() => setIsModalOpen(false)}
        />
      </div>
    </>
  );
};

export default Toolbar;
