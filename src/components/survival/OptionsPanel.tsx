import { DifficultyLevel } from "../../types";
import UIBox from "../ui/UIBox";

type Props = {
  level: DifficultyLevel;
  onChangeLevel: (newLevel: DifficultyLevel) => void;
};

const OptionsPanel = ({ level, onChangeLevel }: Props) => {
  return (
    <UIBox className="p-4 flex flex-col gap-5 md:overflow-auto">
      <div className="flex flex-col gap-3 lg:flex-row lg:gap-2 lg:justify-between lg:items-center">
        <label className="text-slate-800" htmlFor="diff-level">
          Difficulty level
        </label>
        <select
          className="px-3 py-2 lg:w-1/2 rounded ring-1 text-slate-600 ring-slate-400 bg-white flex-grow-0 hover:bg-slate-200/40 focus:outline-none focus:border-solid focus:ring-2 focus:ring-slate-600"
          id="diff-level"
          onChange={(e) => onChangeLevel(e.target.value as DifficultyLevel)}
          value={level}
        >
          <option value="easy">Easy</option>
          <option value="medium">Medium</option>
          <option value="hard">Hard</option>
        </select>
      </div>
    </UIBox>
  );
};

export default OptionsPanel;
