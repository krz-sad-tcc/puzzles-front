import { Advance } from "../../types";
import Answer from "../main-moves/Answer";
import Attempts from "../commons/Attempts";
import UIText from "../ui/UIText";

type Props = {
  advances: Advance[];
  attempts: number;
  rightMoves: number;
  showAllAnswers: boolean;
};

const AnswersPanel = ({
  advances,
  attempts,
  rightMoves,
  showAllAnswers = false,
}: Props) => {
  if (!advances.length) return null;

  const [mainAdvance, ...otherAdvances] = advances;

  return (
    <div className="p-3">
      <div className="flex items-center justify-between">
        <Attempts attempts={attempts} />
        {rightMoves > 0 ? (
          <div className="flex gap-2 items-baseline">
            <UIText variant="dimmed" className="text-sm">
              Score
            </UIText>
            <UIText className="text-2xl" variant="display">
              {rightMoves}
            </UIText>
          </div>
        ) : null}
      </div>

      {showAllAnswers ? (
        <div className="mt-5">
          <Answer answer={mainAdvance} isGuessed={true} />
        </div>
      ) : null}

      {showAllAnswers && otherAdvances.length ? (
        <>
          <UIText className="mt-5" variant="primary">
            Other popular choices
          </UIText>
          <ul className="mt-3 grid grid-cols-4 gap-3">
            {otherAdvances.map((advance) => (
              <li
                key={advance.move._id}
                className="grid place-items-center font-semibold p-2 ring-1 ring-slate-700/10 rounded-lg"
              >
                <UIText variant="dimmed">{advance.move.moveSan}</UIText>
              </li>
            ))}
          </ul>
        </>
      ) : null}
    </div>
  );
};

export default AnswersPanel;
