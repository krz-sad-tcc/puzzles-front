import { useEffect, useRef, useState } from "react";
import { useQuery } from "react-query";
import { chess } from "../../chess";
import {
  Advance,
  ChessToolMove,
  Color,
  History,
  Position,
  PuzzleSurvivalStep,
  PuzzleStatus,
  Turn,
  PuzzleSettings,
} from "../../types";
import { getPositionHistory, getPuzzleSurvivalStep } from "../../api";
import { Queries } from "../../queries";
import { MAX_ATTEMPTS } from "../../constants/puzzle";
import Board from "../commons/Board";
import HistoryPanel from "../commons/HistoryPanel";
import ReferencePanel from "../commons/ReferencePanel";
import UIPuzzleLost from "../ui/UIPuzzleLost";
import UIPuzzleWon from "../ui/UIPuzzleWon";
import AnswersPanel from "./AnswersPanel";
import ControlPanel from "../commons/ControlPanel";
import UIBox from "../ui/UIBox";

type Props = PuzzleSettings;

const Survival = (puzzleSettings: Props) => {
  const [attempts, setAttempts] = useState(MAX_ATTEMPTS);
  const [history, setHistory] = useState<History>();
  const [position, setPosition] = useState<Position>(
    puzzleSettings.rootPosition
  );
  const [puzzleStatus, setPuzzleStatus] = useState<PuzzleStatus>(
    PuzzleStatus.Playing
  );
  const [timeTravelAdvance, setTimeTravelAdvance] = useState<Advance | null>(
    null
  );

  const computerAdvance = useRef<Advance>();
  const playerAdvances = useRef<Advance[]>();
  const rightMoves = useRef(0);
  const turn = useRef<Turn>(Turn.Player);

  const puzzleQuery = useQuery(
    [Queries.getPuzzleSurvivalStep],
    () =>
      getPuzzleSurvivalStep({
        computer: turn.current === Turn.Computer,
        epd: chess.getEpd(),
        level: puzzleSettings.level,
      }),
    {
      onSuccess: onPuzzleStep,
      enabled: false,
    }
  );

  const historyQuery = useQuery(
    [Queries.getPositionHistory],
    () => getPositionHistory({ epd: puzzleSettings.rootPosition.epd }),
    {
      onSuccess: setHistory,
      enabled: false,
    }
  );

  async function startPuzzle() {
    const positionColor: Color =
      puzzleSettings.rootPosition.fen.split(" ")[1] === "b" ? "black" : "white";

    if (positionColor === puzzleSettings.color) {
      turn.current = Turn.Player;
    } else {
      turn.current = Turn.Computer;
    }

    setAttempts(MAX_ATTEMPTS);
    setPosition(puzzleSettings.rootPosition);
    setPuzzleStatus(PuzzleStatus.Playing);
    setTimeTravelAdvance(null);
    rightMoves.current = 0;
    chess.load(puzzleSettings.rootPosition.fen);

    await historyQuery.refetch();
    puzzleQuery.refetch();
  }

  // INFO: every time something changes in the settings, a new puzzle has to be created from scratch.
  useEffect(() => {
    startPuzzle();
  }, [puzzleSettings]);

  function onPuzzleStep(puzzleStep: PuzzleSurvivalStep) {
    if (!puzzleStep.playerAdvances) {
      setPuzzleStatus(PuzzleStatus.Won);
      return;
    }

    computerAdvance.current = puzzleStep.computerAdvance;
    playerAdvances.current = puzzleStep.playerAdvances;

    if (turn.current === Turn.Computer) {
      setTimeout(() => {
        executeComputerMove(computerAdvance.current.move.moveSan);
      }, 1400);
    }
  }

  function executeComputerMove(san: string) {
    chess.move(san);
    turn.current = Turn.Player;

    setHistory({
      ...history,
      advances: [...history.advances, computerAdvance.current],
    } as History);
    setPosition(computerAdvance.current.position);
  }

  function handlePlayerMove(move: ChessToolMove) {
    turn.current = Turn.Computer;

    const index = playerAdvances.current.findIndex(
      (a) => move.san === a.move.moveSan
    );

    // The move is the most played.
    if (index === 0) {
      console.log("Main line.");

      const advance = playerAdvances.current[index];

      setHistory({
        ...history,
        advances: [...history.advances, advance],
      } as History);
      setPosition(advance.position);
      rightMoves.current++;

      puzzleQuery.refetch();
    } else {
      // The move is not in the database.
      if (index === -1) {
        setPosition(undefined);
        console.log("Unknown move.");
      }

      // The move is known, but not the most played.
      else {
        const advance = playerAdvances.current[index];

        setPosition(advance.position);

        const nGames = advance.move.nGames;
        let eco, name;

        if (advance.position.parentName) {
          eco = advance.position.parentEco;
          name = advance.position.parentName;
        } else {
          eco = advance.position.eco;
          name = advance.position.name;
        }

        console.log(
          "Known move: " + nGames + " games - " + eco + " " + name + "."
        );
      }

      setAttempts((prev) => prev - 1);

      if (attempts === 1) {
        setPuzzleStatus(PuzzleStatus.Lost);
        return;
      }

      setTimeout(() => {
        chess.undo();
        setPosition(
          computerAdvance.current?.position || puzzleSettings.rootPosition
        );
      }, 700);
    }
  }

  function handleRestartPuzzle() {
    startPuzzle();
  }

  function handleGoBackInHistory(advance: Advance) {
    if (advance.position.fen === chess.fen()) {
      setTimeTravelAdvance(null);
    } else {
      setTimeTravelAdvance(advance);
    }
  }

  return (
    <div>
      {puzzleQuery.isError ? (
        <div>Error fetching puzzle</div>
      ) : (
        <div className="grid gap-4 grid-cols-1 md:grid-cols-[2fr_4fr_3fr]">
          <div className="flex flex-col gap-4">
            <ReferencePanel position={position} />
            {puzzleStatus === PuzzleStatus.Won ? (
              <UIPuzzleWon onRestartPuzzle={handleRestartPuzzle}>
                <p>
                  You got{" "}
                  <b>
                    {rightMoves.current}{" "}
                    {rightMoves.current > 1 ? "moves" : "move"}
                  </b>{" "}
                  right!
                </p>
              </UIPuzzleWon>
            ) : puzzleStatus === PuzzleStatus.Lost ? (
              <UIPuzzleLost onRestartPuzzle={handleRestartPuzzle}>
                {rightMoves.current > 0 ? (
                  <p>
                    You did your best and even got{" "}
                    <b>
                      {rightMoves.current}{" "}
                      {rightMoves.current > 1 ? "moves" : "move"}
                    </b>{" "}
                    right. Don&apos;t give up!
                  </p>
                ) : (
                  <p>
                    You did your best and that matters a lot! Don&apos;t give
                    up!
                  </p>
                )}
              </UIPuzzleLost>
            ) : null}
          </div>
          <div className="relative">
            <Board
              fen={timeTravelAdvance?.position?.fen || chess.fen()}
              isLocked={
                puzzleStatus !== PuzzleStatus.Playing ||
                Boolean(timeTravelAdvance)
              }
              lastMove={timeTravelAdvance ? undefined : chess.lastMove()}
              onMove={handlePlayerMove}
              orientation={puzzleSettings.color}
            />
          </div>
          <aside>
            <UIBox className="flex flex-col">
              <HistoryPanel
                advances={history?.advances || []}
                currEpd={timeTravelAdvance?.position?.epd || chess.getEpd()}
                onClickItem={handleGoBackInHistory}
              />
              <hr className="mb-2" />
              <AnswersPanel
                advances={playerAdvances.current || []}
                attempts={attempts}
                rightMoves={rightMoves.current}
                showAllAnswers={puzzleStatus !== PuzzleStatus.Playing}
              />
              <hr className="my-2" />
              <ControlPanel
                buttonText="Restart puzzle"
                onClick={handleRestartPuzzle}
              />
            </UIBox>
          </aside>
        </div>
      )}
    </div>
  );
};

export default Survival;
