import { useQuery } from "react-query";
import { useState } from "react";
import { getPosition } from "../api";
import { INITIAL_EPD } from "../constants/chess";
import { Position, PuzzleSettings, PuzzleType } from "../types";
import { Queries } from "../queries";
import MainMoves from "./main-moves/MainMoves";
import Survival from "./survival/Survival";
import Toolbar from "./Toolbar";
import Footer from "./commons/Footer";
import Header from "./commons/Header";

const App = () => {
  const [settings, setSettings] = useState<PuzzleSettings>({
    rootPosition: {} as Position,
    puzzleType: PuzzleType.MainMoves,
    level: "easy",
    color: "white",
  });

  const initialPositionQuery = useQuery(
    [Queries.getInitialPosition],
    () =>
      getPosition({
        epd: INITIAL_EPD,
      }),
    {
      onSuccess: (initialPosition) =>
        setSettings((prev) => ({ ...prev, rootPosition: initialPosition })),
    }
  );

  return (
    <div className="overflow-x-hidden">
      <Header />
      {initialPositionQuery.isFetched ? (
        <main className="max-w-screen-xl m-auto">
          <Toolbar puzzleSettings={settings} onChangeSettings={setSettings} />
          <div className="mt-4">
            {settings.puzzleType === PuzzleType.Survival ? (
              <Survival {...settings} />
            ) : (
              <MainMoves {...settings} />
            )}
          </div>
        </main>
      ) : null}
      <Footer />
    </div>
  );
};

export default App;
