import { IoHelpCircle } from "react-icons/io5";
import { PuzzleMainMovesAnswer } from "../../types";

type Props = {
  answer: PuzzleMainMovesAnswer;
};

const UIMovePlaceholder = ({ answer }: Props) => (
  <div className="flex gap-4 items-center p-3 bg-white ring-1 ring-slate-700/10 rounded-lg border-black/10 min-h-[70px]">
    <div className="w-3/12 flex-grow-0 flex-shrink-0 flex justify-start">
      <IoHelpCircle
        size={24}
        className="text-slate-400"
        title={answer.move.moveSan}
      />
    </div>
    <div className="w-8/12">
      <div className="w-3/12 h-[8px] rounded-lg bg-slate-200" />
      <div className="w-10/12 h-[6px] rounded-lg bg-slate-200 mt-2" />
      <div className="w-8/12 h-[6px] rounded-lg bg-slate-200 mt-1" />
    </div>
  </div>
);

export default UIMovePlaceholder;
