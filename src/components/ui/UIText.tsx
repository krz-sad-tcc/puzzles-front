import classNames from "classnames";

type TextVariant = "display" | "primary" | "secondary" | "dimmed";

type Props = {
  children: React.ReactNode;
  className?: string;
  variant: TextVariant;
};

const UIText = ({ children, className, variant }: Props) => {
  const classes = classNames(className, {
    "font-bold text-slate-900": variant === "display",
    "font-bold text-pink-600": variant === "primary",
    "text-slate-800": variant === "secondary",
    "text-slate-500": variant === "dimmed",
  });
  return <p className={classes}>{children}</p>;
};

export default UIText;
