import classNames from "classnames";
import React from "react";

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  className?: string;
  children: React.ReactNode;
  onClick: (evt: React.MouseEvent | React.FormEvent) => void;
  size?: "small" | "normal";
  type?: "button" | "submit";
  variant?: "normal" | "success" | "warning" | "danger";
};

const UIButton = ({
  className,
  children,
  onClick,
  size = "normal",
  type = "button",
  variant = "normal",
  ...props
}: Props) => {
  const classes = classNames(
    "flex justify-center items-center gap-2 rounded-md font-semibold transition ease-in duration-20",
    className,
    {
      "text-pink-500 border-2 border-transparent ring-1 ring-pink-500 hover:bg-pink-600/20 hover:text-pink-700 hover:outline-none hover:bg-pink-300/50 focus:outline-none focus:border-2 focus:border-pink-600/50 focus:ring-4 focus:ring-pink-500/30 active:bg-pink-800 active:text-white":
        variant === "normal",
      "text-white border-2 border-transparent ring-1 ring-transparent hover:bg-red-700/40 hover:text-white hover:outline-none focus:outline-none focus:ring-4 focus:ring-white/60 active:bg-red-800/60 active:text-white":
        variant === "danger",
      "text-white border-2 border-transparent ring-1 ring-transparent hover:bg-yellow-700/40 hover:text-white hover:outline-none focus:outline-none focus:ring-4 focus:ring-white/60 active:bg-yellow-800/60 active:text-white":
        variant === "warning",
      "text-white border-2 border-transparent ring-1 ring-transparent hover:bg-green-700/40 hover:text-white hover:outline-none focus:outline-none focus:ring-4 focus:ring-white/60 active:bg-green-800/60 active:text-white":
        variant === "success",
      "py-2 px-4": size === "normal",
      "py-1 px-2 text-sm": size === "small",
    }
  );

  return (
    <button className={classes} onClick={onClick} type={type} {...props}>
      {children}
    </button>
  );
};

export default UIButton;
