type Props = {
  checked: string;
  id: string;
  label?: string;
  onChange: (option: string) => void;
  options: { id: string; label: string }[];
};

const UIRadioGroup = ({ checked, id, label, onChange, options }: Props) => {
  return (
    <fieldset className="flex items-center">
      <legend className="text-slate-800">{label}</legend>
      <div className="flex gap-3 justify-center w-full sm:justify-start">
        {options.map((opt) => (
          <div className="form-check" key={`${id}-${opt.id}`}>
            <input
              className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-white checked:border-4 checked:border-pink-600 focus:outline-none focus:ring-4 focus:border-1 focus:border-pink-600 focus:ring-pink-500/30 transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
              type="radio"
              name={id}
              id={`${id}-${opt.id}`}
              checked={opt.id === checked}
              onChange={() => onChange(opt.id)}
            />
            <label
              className="form-check-label hover:cursor-pointer inline-block text-slate-800"
              htmlFor={`${id}-${opt.id}`}
            >
              {opt.label}
            </label>
          </div>
        ))}
      </div>
    </fieldset>
  );
};
export default UIRadioGroup;
