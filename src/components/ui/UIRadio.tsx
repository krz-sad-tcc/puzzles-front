import classNames from "classnames";
import { ReactElement } from "react";

type Props = {
  checked: boolean;
  children?: ReactElement;
  className?: string;
  id: string;
  label?: string;
  name: string;
  onChange: () => void;
  variant: "horizontal" | "vertical";
};

const UIRadio = ({
  checked,
  children,
  className,
  id,
  label,
  name,
  onChange,
  variant,
}: Props) => {
  const classes = classNames("form-check flex", className, {
    "flex-row": variant === "horizontal",
    "flex-col": variant === "vertical",
  });

  return (
    <div className={classes}>
      <input
        className="form-check-input flex-shrink-0 appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-white checked:border-[4px] checked:border-pink-600 focus:outline-none focus:ring-4 focus:border-1 focus:border-pink-600 focus:ring-pink-500/30 transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
        type="radio"
        name={name}
        id={id}
        checked={checked}
        onChange={() => onChange()}
      />
      <label
        className="form-check-label hover:cursor-pointer inline-block text-slate-800"
        htmlFor={id}
      >
        {children || label}
      </label>
    </div>
  );
};

export default UIRadio;
