import classNames from "classnames";
import "../../assets/ui-loading.css";

type Props = {
  className?: string;
};

const UILoading = ({ className }: Props) => {
  return (
    <div className={classNames("ui-loading", className)}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default UILoading;
