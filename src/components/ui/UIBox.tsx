import { CSSProperties } from "react";
import classnames from "classnames";

type Props = {
  children: React.ReactNode;
  className?: string;
  style?: CSSProperties;
  tag?: keyof HTMLElementTagNameMap;
};

const UIBox = ({ children, className, style }: Props) => {
  const classes = classnames(
    className,
    "bg-white ring-1 ring-slate-700/10 rounded-lg shadow-lg border-black/10"
  );

  return (
    <div className={classes} style={style}>
      {children}
    </div>
  );
};

export default UIBox;
