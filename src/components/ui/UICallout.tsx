import { IoInformationCircle } from "react-icons/io5";

type Props = { children: string };

const UICallout = ({ children }: Props) => {
  return (
    <p className="mt-3 flex gap-3 rounded-md px-3 py-2 text-gray-500 bg-slate-200/50">
      <IoInformationCircle className="mt-1 flex-shrink-0" />
      <span>{children}</span>
    </p>
  );
};

export default UICallout;
