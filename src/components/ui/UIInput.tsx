import React from "react";

type Props = {
  ariaLabel?: string;
  id: string;
  label?: string;
  onChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
};

const UIInput = ({ ariaLabel, id, label, onChange, value }: Props) => {
  return (
    <div className="flex flex-col gap-3">
      {label && (
        <label className="text-slate-800" htmlFor={id}>
          {label}
        </label>
      )}
      <input
        className="py-2 px-2 text-slate-600 rounded-md transition ease-in duration-20 border border-slate-300 focus:border-pink-600/50 focus:ring-4 focus:ring-pink-500/30 outline-none"
        id={id}
        value={value}
        onChange={onChange}
        aria-label={ariaLabel}
      />
    </div>
  );
};

export default UIInput;
