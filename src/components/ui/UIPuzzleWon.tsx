import { ReactElement } from "react";
import UIBox from "./UIBox";
import UIButton from "./UIButton";

type Props = {
  children?: ReactElement | ReactElement[];
  onRestartPuzzle: () => void;
};

const UIPuzzleWon = ({ children, onRestartPuzzle }: Props) => {
  return (
    <UIBox className="flex flex-col justify-between p-5 relative bg-green-600/75 text-white">
      <div className="relative">
        <h2 className="mb-10 font-bold">
          <span className="inline-block text-xl">‘٩꒰｡•◡•｡꒱۶’</span>
          <span className="inline-block text-2xl mt-2">Congratulations!</span>
        </h2>
        {children}
      </div>
      <div className="flex flex-col justify-between gap-2 mt-5">
        <UIButton
          className="w-full"
          variant="success"
          onClick={onRestartPuzzle}
        >
          Restart
        </UIButton>
      </div>
    </UIBox>
  );
};

export default UIPuzzleWon;
