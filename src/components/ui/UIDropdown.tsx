import { Listbox, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { IoCheckmark, IoChevronDown } from "react-icons/io5";
import classNames from "classnames";

interface Props<T> {
  className?: string;
  label: string;
  options: {
    label: string;
    value: T;
  }[];
  value: T;
  onChange: (newValue: T) => void;
}

function UIDropdown<T>({
  className,
  label,
  options,
  value,
  onChange,
}: Props<T>) {
  const selectedLabel = options.find((o) => o.value === value).label;

  return (
    <div className={className}>
      <Listbox value={value} onChange={onChange}>
        {({ open }) => (
          <>
            <Listbox.Button className="relative w-full cursor-default rounded-md border border-gray-300 bg-white py-2 pl-3 pr-10 text-left shadow-sm focus:border-pink-500 focus:outline-none focus:ring-1 focus:ring-pink-500 sm:text-sm">
              <span className="text-slate-500">{label}:</span>
              <span className="font-medium capitalize inline-block ml-1">
                {selectedLabel}
              </span>
              <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                <IoChevronDown
                  className={classNames(
                    "h-5 w-5 text-slate-400/70 transition ease-in-out duration-100",
                    { "rotate-180": open }
                  )}
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Listbox.Options className="absolute origin-top-left z-10 mt-1 max-h-56 w-40 overflow-auto rounded-md bg-white p-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                {options.map((option) => (
                  <Listbox.Option
                    key={JSON.stringify(option.value)}
                    value={option.value}
                    className={({ active }) =>
                      classNames(
                        active
                          ? "text-pink-800 bg-pink-200/50 rounded"
                          : "text-gray-900",
                        "relative cursor-default select-none py-2 pr-3 pl-6"
                      )
                    }
                  >
                    {({ selected }) => (
                      <>
                        {selected ? (
                          <span
                            className={classNames(
                              "text-pink-600",
                              "absolute inset-y-0 left-0 flex items-center pl-2"
                            )}
                          >
                            <IoCheckmark
                              className="h-5 w-5"
                              aria-hidden="true"
                            />
                          </span>
                        ) : null}
                        <div className="flex items-center">
                          <span
                            className={classNames(
                              selected ? "font-bold" : "font-normal",
                              "ml-3 block truncate capitalize"
                            )}
                          >
                            {option.label}
                          </span>
                        </div>
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </>
        )}
      </Listbox>
    </div>
  );
}

export default UIDropdown;
