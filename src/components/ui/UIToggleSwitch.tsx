import classNames from "classnames";
import React, { useCallback } from "react";

type Props = {
  enabled: boolean;
  name: string;
  onToggle: () => void;
  label: string;
};

const UIToggleSwitch = ({ enabled, name, onToggle, label }: Props) => {
  const wrapperClasses = classNames(
    "bg-slate-400 rounded-full relative h-[24px] w-[40px] flex-shrink-0 cursor-pointer transition ease-in-out duration-50 focus:outline-none focus:border-solid focus:ring-2 focus:ring-slate-600",
    {
      "bg-green-500 focus:ring-green-700": enabled,
    }
  );
  const togglerClasses = classNames(
    "bg-white rounded-full h-[18px] w-[18px] absolute top-[3px] left-[3px] transition-all ease-in duration-20",
    {
      "left-[18px]": enabled,
    }
  );

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent) => {
      if (event.code === "Space" || event.code === "Enter") {
        onToggle();
      }
    },
    [onToggle]
  );
  const labelId = name + "-label";

  return (
    <div className="flex flex-row gap-3 items-center">
      <div
        className={wrapperClasses}
        tabIndex={0}
        role="switch"
        aria-checked={enabled}
        aria-labelledby={labelId}
        onClick={onToggle}
        onKeyDown={handleKeyDown}
      >
        <div className="h-full w-full m-0 relative cursor-pointer appearance-none" />
        <div className={togglerClasses} />
      </div>
      <p
        className="text-slate-800 leading-tight break-words whitespace-normal"
        id={labelId}
      >
        {label}
      </p>
    </div>
  );
};

export default UIToggleSwitch;
