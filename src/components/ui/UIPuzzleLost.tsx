import { ReactElement } from "react";
import classNames from "classnames";
import UIButton from "./UIButton";

type Props = {
  children?: ReactElement | ReactElement[];
  onRestartPuzzle: () => void;
};

const UIPuzzleLost = ({ children, onRestartPuzzle }: Props) => {
  const classes = classNames(
    "flex flex-col justify-between p-5 rounded-lg text-white bg-red-600/75"
  );

  return (
    <div className={classes}>
      <div className="relative">
        <h2 className="text-xl font-bold mb-10">{"Oh no! ( > ~ < )"}</h2>
        {children}
      </div>
      <div className="flex flex-col justify-between gap-1 mt-2">
        <UIButton variant="danger" onClick={onRestartPuzzle}>
          Restart
        </UIButton>
      </div>
    </div>
  );
};

export default UIPuzzleLost;
