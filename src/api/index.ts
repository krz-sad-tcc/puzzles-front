import {
  Color,
  DifficultyLevel,
  History,
  Position,
  PuzzleMainMoves,
  PuzzleSurvivalStep,
} from "../types";

const apiUrl = import.meta.env.VITE_API_URL;

export const getPuzzleMainMoves = async (params: {
  color: Color;
  level: DifficultyLevel;
  rootEpd: string;
}): Promise<PuzzleMainMoves> => {
  const response = await fetch(
    `${apiUrl}/puzzles/main-moves?color=${params.color}&level=${params.level}&rootEpd=${params.rootEpd}`
  );
  return response.json();
};

export const getPuzzleSurvivalStep = async (params: {
  computer: boolean;
  epd: string;
  level: DifficultyLevel;
}): Promise<PuzzleSurvivalStep> => {
  const response = await fetch(
    `${apiUrl}/puzzles/survival?computer=${params.computer}&epd=${params.epd}&level=${params.level}`
  );
  return response.json();
};

export const getPosition = async (params: {
  epd: string;
}): Promise<Position> => {
  const response = await fetch(`${apiUrl}/positions?epd=${params.epd}`);
  return response.json();
};

export const getPositionHistory = async (params: {
  epd: string;
}): Promise<History> => {
  const response = await fetch(`${apiUrl}/positions/history?epd=${params.epd}`);
  return response.json();
};

export const getPositionInfo = async (params: {
  key: string;
}): Promise<string | null> => {
  const response = await fetch(`${apiUrl}/positions/${params.key}/info`);
  const json = (await response.json()) as { info: string | null };
  return json.info;
};

export const getOpeningSuggestions = async (params: {
  term: string;
}): Promise<Position[]> => {
  const queryString = `term=${params.term}`;
  const response = await fetch(`${apiUrl}/positions/search?${queryString}`);
  const json = (await response.json()) as { positions: Position[] };
  return json.positions;
};
