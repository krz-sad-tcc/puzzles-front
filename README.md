# Still Theory Frontend

This is the client side of **Still Theory**, a web application that provides chess openings puzzles. It is part of the CS graduation [final project][1] by Renan Krzesinski and Ygor Sad Machado.

## Technologies

- Frontend framework: [React][2]
- CSS framework: [Tailwind][3]
- Fetching: [React Query][4]

## Requirements

- [Still Theory API][5]
- [Node.js 16][6]

We recommend the use of [NVM][7] to manage Node.js versions.

## Dev

Clone the project, copy the file `.env.example` and name the new file `.env`. Modify it according to your needs. Firt run the API, Then run

```bash
yarn run dev
```

The app should now be available on http://localhost:3000/.

[1]: https://linux.ime.usp.br/~renankrz/
[2]: https://reactjs.org/
[3]: https://tailwindui.com/
[4]: https://react-query-v3.tanstack.com/overview
[5]: https://gitlab.com/krz-sad-tcc/puzzles-api
[6]: https://nodejs.org/en/
[7]: https://github.com/nvm-sh/nvm
